#-------------------------------------------------------------------------------
# Name:        pygeoisland
# Purpose:     GUI and addons for Script ISland SEGMENTATION v2.2
#
#              Computer Vision Module Script ISland SEGMENTATION v2.2
#              Authored by Luis Sanchez [Anma Technologies]
#
# Author:      Adolfo J. Cardozo S. ( GUI and addons)
# Created:     April 2020
# Copyright:   (c) Adolfo J. Cardozo S. 2020 [ACAI Engineering ia]
#-------------------------------------------------------------------------------
import tkinter as tk
import pygeosupport as pgs

def main():
    global files
    # setup application
    app = tk.Tk()
    app.withdraw()

    splash = pgs.show_splash(app)

    pgs.setup_app(app)
    pgs.init_vars(app)
    pgs.create_menu(app)
    # work area
    pgs.wrkinit(app)
    pgs.create_widgets(app)
    app.status.set("Ready.")

    splash.destroy()

    app.deiconify()
    app.mainloop()

if __name__ == '__main__':
    main()
