#-------------------------------------------------------------------------------
# Name:        pygeosupport
# Purpose:     Support module for GUI and addons of pyGeoIsland
# Author:      Adolfo J. Cardozo S.
# Created:     April 2020
# Copyright:   (c) Adolfo J. Cardozo S. 2020 [ACAI Engineering ia]
#-------------------------------------------------------------------------------
import os
import glob
import json
import tkinter as tk
import tkinter.ttk as ttk
import PIL.Image, PIL.ImageTk
import cv2
import geometryIsland as GI #this is a New Module that was builded for the project
import pandas as pd

from time import sleep
from tkinter.filedialog import askdirectory
from tkinter.messagebox import showerror, showinfo

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import shapefile as shp
from openpyxl import load_workbook

np = pd.np

__VERSION__ = "2.4"
__APP_NAME_SHORT__ = f"pyGeoIsland v.{__VERSION__}"
__APP_NAME_LONG__ = f"Island Segmentation and Geometry v{__VERSION__}"

def show_splash(app):
    splash = tk.Toplevel(app)
    splash.title("Splash")
    scr_size = (app.winfo_screenwidth(), int(0.9*app.winfo_screenheight()))
    win_size = (width,height) = (400, 300)
    win_pos = (x_pos,y_pos) = tuple(map(pos_center,scr_size,win_size,(2,2)))
    splash.geometry(f'{width}x{height}+{x_pos}+{y_pos}')
    frm = tk.Frame(splash, relief=tk.SOLID, bd=2)
    frm.pack(padx=20, pady=20, fill=tk.BOTH, expand=True)
    tk.Label(frm, text=__APP_NAME_SHORT__, font=("Helvetica", 23)).pack(side=tk.TOP)
    splash.progressbar = ttk.Progressbar(frm, orient=tk.HORIZONTAL)
    splash.progressbar.pack(side=tk.BOTTOM, fill=tk.X, padx=10, pady=5)
    can = tk.Canvas(frm, relief=tk.SOLID, bd=1, bg='white')
    img = PIL.ImageTk.PhotoImage(image = PIL.Image.open('resources/gis-syst.jpg'))
    can.create_image(int(313/2+21), int(161/2+11), image=img)
    can.image = img
    can.pack(fill=tk.BOTH, expand=True)
    splash.transient()
    splash.overrideredirect(True)
    for i in range(5):
        splash.progressbar['value'] = (i+1) * 100/5
        splash.update()
        sleep(0.1)
    sleep(0.11)
    return splash

def valid_num(value):
    try:
        float(value)
    except ValueError:
        return False
    return True

pos_center = lambda a,b,c: int((a-b) // c)

def setup_app(app):
    scr_size = (app.winfo_screenwidth(), int(0.9*app.winfo_screenheight()))
    win_size = (width,height) = (800, 600)
    win_pos = (x_pos,y_pos) = tuple(map(pos_center,scr_size,win_size,(2,2)))
    app.geometry(f'{width}x{height}+{x_pos}+{y_pos}')
    app.resizable(False, False)
    app.title(__APP_NAME_LONG__)
    app.iconbitmap(default='resources/pygeoisland.ico')

def init_vars(app):
    app.status = tk.StringVar()
    app.msg = tk.StringVar()

    app.prj_path = tk.StringVar()
    app.east_upper_right = tk.DoubleVar()
    app.north_upper_right = tk.DoubleVar()
    app.east_lower_left = tk.DoubleVar()
    app.north_lower_left = tk.DoubleVar()
    app.kval = tk.IntVar()
    app.method = tk.IntVar()
    app.processed = tk.IntVar()
    app.files = []
    app.types = {'img': ['images', '', '.tif'],
                 'con': ['contourn', 'poly_', '.tif'],
                 'clu': ['cluster', 'cluster_', '.tif'],
                 'sha': ['shapefiles', 'polygon_', '.shp']}

    app.east_upper_right.set(602889.0)
    app.north_upper_right.set(5938009.0)
    app.east_lower_left.set(588978.0)
    app.north_lower_left.set(5732748.0)
    app.kval.set(6)
    app.method.set(1)
    app.processed.set(False)

def create_menu(app):
    # close callback
    def _close():
        app.destroy()
        app.quit()

    # create menu
    menu = tk.Menu(app)
    file_menu = tk.Menu(menu, tearoff=False)
    file_menu.add_command(label="New", underline=0, accelerator="Ctrl+N", command=lambda x=app: new_dataset(x))
    file_menu.add_command(label="Open...", underline=0, accelerator="Ctrl+O", command=lambda x=app: open_dataset(x))
    file_menu.add_command(label="Save", underline=0, accelerator="Ctrl+S", command=lambda x=app: save_dataset(x))
    file_menu.add_separator()
    file_menu.add_command(label="Exit", underline=1, accelerator="Alt+F4", command=_close)
    menu.add_cascade(label="File", underline=0, menu=file_menu)
    help_menu = tk.Menu(menu, tearoff=False)
    help_menu.add_command(label="Guide", underline=0, command=guide)
    help_menu.add_separator()
    help_menu.add_command(label="About", underline=0, command=lambda x=app: about(x))
    menu.add_cascade(label="Help", underline=0, menu=help_menu)
    app.config(menu=menu)

def wrkinit(app):
    app.columnconfigure(0, weight=1)
    app.columnconfigure(1, weight=6)
    app.columnconfigure(2, weight=6)
    app.columnconfigure(3, weight=3)
    app.rowconfigure(0, weight=1)
    app.rowconfigure(1, weight=20)
    app.rowconfigure(2, weight=1)

def create_widgets(app):
    # Toolbar
    lbl1 = ttk.Label(app, relief=tk.SOLID, textvariable=app.prj_path)
    btn1 = ttk.Button(app, text="Dataset Folder...", command=lambda x=app: open_project(x))
    btn2 = ttk.Button(app, text="Process Images...", state=tk.DISABLED, command=lambda x=app: process_images(x))
    lbl2 = ttk.Label(app, relief=tk.SOLID, textvariable=app.msg)
    # Image area
    cnv1 = tk.Canvas(app, bg="gray95", relief=tk.SOLID, bd=1)
    # Left Panel
    frm1 = tk.Frame(app, bg="gray95", relief=tk.SOLID, bd=1)
    # Data entry: coordinates of reference, K value of K-means and Method of processing the dataset images
    frm2 = tk.Frame(frm1, bg="gray95")
    frm5 = tk.Frame(frm2, bg="gray95")
    lbl7 = tk.Label(frm5, text="East", width=9)
    lbl8 = tk.Label(frm5, text="North", width=9)
    frm3 = tk.Frame(frm2, bg="gray95")
    lbl4 = tk.Label(frm3, text="Upper-Right:")
    # Coordinates of reference points
    # Upper Right point
    app.urce = ent4 = ttk.Entry(frm3, width=10, textvariable=app.east_upper_right, justify=tk.RIGHT,
        validate='key', validatecommand=(app.register(valid_num), "%P"), state=tk.DISABLED)
    app.urcn = ent5 = ttk.Entry(frm3, width=10, textvariable=app.north_upper_right, justify=tk.RIGHT,
        validate='key', validatecommand=(app.register(valid_num), "%P"), state=tk.DISABLED)
    frm4 = tk.Frame(frm2, bg="gray95")
    lbl6 = tk.Label(frm4, text="Lower-Left :")
    # Lower Left point
    app.llce = ent6 = ttk.Entry(frm4, width=10, textvariable=app.east_lower_left, justify=tk.RIGHT,
        validate='key', validatecommand=(app.register(valid_num), "%P"), state=tk.DISABLED)
    app.llcn = ent7 = ttk.Entry(frm4, width=10, textvariable=app.north_lower_left, justify=tk.RIGHT,
        validate='key', validatecommand=(app.register(valid_num), "%P"), state=tk.DISABLED)
    # K value spin
    frm8 = tk.Frame(frm2, bg="gray95")
    lbl9 = tk.Label(frm8, text="K Value:", justify=tk.LEFT)
    app.spin = tk.Spinbox(frm8, width=9, from_=3, to=7, justify=tk.RIGHT, textvariable=app.kval, state=tk.DISABLED)
    frm9 = tk.Frame(frm8, bg="gray95", width=62)
    # Method of processing selector
    frma = ttk.Labelframe(frm2, text='Select Method of Analysis')
    app.met1 = ttk.Radiobutton(frma, text='Method-1', variable=app.method, value=0, state=tk.DISABLED)
    app.met2 = ttk.Radiobutton(frma, text='Method-2', variable=app.method, value=1, state=tk.DISABLED)
    #app.met3 = ttk.Radiobutton(frma, text='Method-3', variable=app.method, value=2, state=tk.DISABLED)
    # List Box of files to process
    lst1 = tk.Listbox(frm1, relief=tk.SOLID, bd=1, selectmode=tk.BROWSE)
    scr1 = ttk.Scrollbar(frm1, orient=tk.VERTICAL)
    # Actions buttons fr listbox files
    frmb = tk.Frame(frm1, bg="gray95")
    app.datab = ttk.Button(frmb, text="Resume...", state=tk.DISABLED, command=lambda x=app: pop_datab(x))
    app.img = ttk.Button(frmb, text="Image...", state=tk.DISABLED, command=lambda x=app,y='img': pop_image(x,y))
    app.cont = ttk.Button(frmb, text="Contourn...", state=tk.DISABLED, command=lambda x=app,y='con': pop_image(x,y))
    app.clus = ttk.Button(frmb, text="Cluster...", state=tk.DISABLED, command=lambda x=app,y='clu': pop_image(x,y))
    app.shap = ttk.Button(frmb, text="Shapefile...", state=tk.DISABLED, command=lambda x=app,y='sha': pop_image(x,y))
    # Status bar
    lbl3 = ttk.Label(app, relief=tk.SOLID, textvariable=app.status)

    # Packing widgets
    # Toolbar
    btn1.grid(row=0, column=0, padx=5, sticky=tk.E)
    btn2.grid(row=0, column=0, padx=5, sticky=tk.W)
    lbl1.grid(row=0, column=1, columnspan=2, padx=5, sticky=tk.EW)
    lbl2.grid(row=0, column=3, padx=5, sticky=tk.EW)
    # Image area
    cnv1.grid(row=1, column=1, columnspan=3, padx=5, sticky=tk.NSEW)
    # Left panel
    frm1.grid(row=1, column=0, padx=5, sticky=tk.NSEW)
    frm2.pack(side=tk.TOP, fill=tk.BOTH)
    # Coordinates of reference points
    frm5.pack(side=tk.TOP, fill=tk.BOTH)
    lbl8.pack(side=tk.RIGHT, padx=0)
    lbl7.pack(side=tk.RIGHT, padx=0)
    # Upper Right
    frm3.pack(side=tk.TOP, fill=tk.BOTH)
    lbl4.pack(side=tk.LEFT, padx=0)
    ent5.pack(side=tk.RIGHT)
    ent4.pack(side=tk.RIGHT)
    # Lower Left
    frm4.pack(side=tk.TOP, fill=tk.BOTH)
    lbl6.pack(side=tk.LEFT, padx=0)
    ent7.pack(side=tk.RIGHT)
    ent6.pack(side=tk.RIGHT)
    # K value spin
    frm8.pack(side=tk.TOP, fill=tk.BOTH)
    lbl9.pack(side=tk.LEFT, padx=0)
    frm9.pack(side=tk.RIGHT, padx=0)
    app.spin.pack(side=tk.RIGHT, anchor=tk.W)
    # Method of processing selector
    frma.pack(side=tk.TOP, fill=tk.BOTH)
    #app.met3.pack(side=tk.RIGHT, padx=(0, 7))
    app.met2.pack(side=tk.RIGHT, padx=(0, 7))
    app.met1.pack(side=tk.RIGHT, padx=(0, 7))

    # Actions buttons fr listbox files
    frmb.pack(side=tk.RIGHT, fill=tk.BOTH)
    app.datab.pack(side=tk.TOP, padx=10, pady=(20, 0))
    app.shap.pack(side=tk.BOTTOM, padx=10, pady=(0, 20))
    app.clus.pack(side=tk.BOTTOM, padx=10)
    app.cont.pack(side=tk.BOTTOM, padx=10)
    app.img.pack(side=tk.BOTTOM, padx=10)
    # List Box of files to process
    scr1.pack(side=tk.RIGHT, fill=tk.Y, padx=0)
    lst1.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
    # Status bar
    lbl3.grid(row=2, column=0, columnspan=4, padx=5, sticky=tk.EW)
    # setting scrollbar
    lst1.config(yscrollcommand=scr1.set)
    scr1.config(command=lst1.yview)
    lst1.bind('<<ListboxSelect>>', on_select_list)
    lst1.last_select = None

def open_project(app=None):
    dir_path =  os.path.join(os.environ["USERPROFILE"], "Documents")
    #dir_path = os.path.dirname(os.path.abspath(__name__))
    app.status.set("Select a folder...")
    prj_path = askdirectory(initialdir=dir_path)
    if app is not None and len(prj_path) > 0:
        app.prj_path.set(prj_path)
        load_files(app)

def load_files(app):
    prj_path = app.prj_path.get()
    img_path = os.path.normpath(os.path.join(prj_path, 'images'))
    if not os.path.isdir(img_path):
        showerror("Error", "Dataset do not contains a 'images'\ndirectory with images to analize.")
        return
    app.prj_path.set(prj_path)
    app.status.set("Loading files .tif from {}...".format(img_path))
    app.update_idletasks()
    app.files = glob.glob(os.path.join(img_path, "*.tif"))
    app.msg.set("Contains: {} images.".format(len(app.files)))
    listbox = app.children['!frame'].children['!listbox']
    for file in app.files:
        listbox.insert(tk.END, os.path.basename(file))
    listbox.selection_set(0)
    listbox.event_generate('<<ListboxSelect>>')
    app.children['!button2'].config(state=tk.ACTIVE)
    app.urce.config(state=tk.ACTIVE)
    app.urcn.config(state=tk.ACTIVE)
    app.llce.config(state=tk.ACTIVE)
    app.llcn.config(state=tk.ACTIVE)
    app.spin.config(state=tk.NORMAL)
    app.met1.config(state=tk.ACTIVE)
    app.met2.config(state=tk.ACTIVE)
    #app.met3.config(state=tk.ACTIVE)
    app.img.config(state=tk.ACTIVE)
    if app.processed.get():
        app.cont.config(state=tk.NORMAL)
        app.clus.config(state=tk.NORMAL)
        app.shap.config(state=tk.NORMAL)
        app.datab.config(state=tk.NORMAL)

    #app.status.set("Ready.")

def on_select_list(event):
    listbox = event.widget
    select = listbox.curselection()
    if len(select) == 0:
        return
    index = int(select[0])
    if index != listbox.last_select:
        app = listbox.master.master
        value = listbox.get(index)
        listbox.last_select = index
        show_image(app, app.children['!canvas'], value)
        app.status.set("{} selected.".format(value))

def show_image(app, can, value, type_='img'):
    canvas = can # app.children['!canvas']
    canvas.delete(tk.ALL)
    img_path = os.path.join(app.prj_path.get(), app.types[type_][0])
    img = cv2.imread(os.path.join(img_path, value))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, (canvas.winfo_width(), canvas.winfo_height()))
    img = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(img))
    canvas.create_image(0, 0, image=img, anchor=tk.NW)
    canvas.image = img

def show_shape(app, can, value, type_):
    obj_type, prext, ext = app.types['clu']
    img_path = os.path.join(app.prj_path.get(), obj_type)
    img_name = ''.join([prext, os.path.basename(value.split('_')[1]).split('.')[0], ext])
    img = cv2.imread(os.path.join(img_path, img_name))
    ymax, xmax = img.shape[:2]
    shp_path = os.path.join(app.prj_path.get(), app.types[type_][0])
    shapefile = shp.Reader(os.path.join(shp_path, value))
    for shape in shapefile.shapes():
        points = np.array(shape.points)
        intervals = list(shape.parts)+[len(shape.points)]
    fig = plt.Figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.set_aspect(1)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.set_xticklabels([])
    ax.set_xticks([])
    ax.axes.get_xaxis().set_visible(False)
    ax.set_xlim(0, xmax)
    ax.spines['left'].set_visible(False)
    ax.set_yticklabels([])
    ax.set_yticks([])
    ax.axes.get_yaxis().set_visible(False)
    ax.set_ylim(-1*ymax, 0)
    #ax.invert_yaxis()
    ax.margins(0,0)
    ax.set_frame_on(False)
    fig.tight_layout(pad=0.5)
    for (i, j) in zip(intervals[:-1], intervals[1:]):
        pts = points[i:j]
        ax.plot(*zip(*np.append(pts, pts[:1], axis=0)))
    canvas = FigureCanvasTkAgg(fig, master=can)
    canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    can.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

def pop_image(app, type_):
    obj_type, prext, ext = app.types[type_]
    pop = tk.Toplevel(app)
    scr_size = (app.winfo_screenwidth(), int(0.9*app.winfo_screenheight()))
    win_size = (width,height) = (520,520)
    win_pos = (x_pos,y_pos) = tuple(map(pos_center,scr_size,win_size,(2,2)))
    pop.geometry(f'{width}x{height}+{x_pos}+{y_pos}')
    pop.resizable(False, False)

    def _pop_close():
        pop.destroy()

    listbox = app.children['!frame'].children['!listbox']
    idx = listbox.curselection()[0]
    file = app.files[idx]
    filename =  ''.join([prext, os.path.basename(file).split('.')[0], ext])
    pop.title('Preview {} - {}'.format(obj_type, file))
    canvas = tk.Canvas(pop, bg="gray95", relief=tk.SOLID, bd=1)
    canvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    canvas.update()
    if type_ in ('img', 'clu', 'con'):
        show_image(app, canvas, filename, type_)
    elif type_ == 'sha':
        show_shape(app, canvas, filename, type_)
    pop.transient(app)
    pop.grab_set()
    pop.focus()
    pop.protocol("WM_DELETE_WINDOW", _pop_close)

def pop_datab(app):
    pop = tk.Toplevel(app)
    scr_size = (app.winfo_screenwidth(), int(0.9*app.winfo_screenheight()))
    win_size = (width,height) = (750,550)
    win_pos = (x_pos,y_pos) = tuple(map(pos_center,scr_size,win_size,(2,2)))
    pop.geometry(f'{width}x{height}+{x_pos}+{y_pos}')
    pop.resizable(False, False)

    def _pop_close():
        pop.destroy()

    filename = os.path.join(app.prj_path.get(), "dataBase.xlsx")
    pop.title('Preview {} - {}'.format("Geometrical DB", filename))

    scroll_vert = ttk.Scrollbar(pop, orient=tk.VERTICAL)
    scroll_hori = ttk.Scrollbar(pop, orient=tk.HORIZONTAL)
    treeview = ttk.Treeview(pop, show='headings', selectmode=tk.BROWSE)  # , style="mystyle.Treeview")

    workbook = load_workbook(filename)
    sheet = workbook.active
    columns = []
    heading_setted = False
    for nrow, row in enumerate(sheet.iter_rows()):
        if not heading_setted :
            for cell in row:
                columns.append(cell.value)
            treeview.config(columns=columns)
            treeview.column('#0', minwidth=0, stretch=0, width=0)
            for column in columns:
                treeview.column(column, anchor=tk.W, minwidth=70, stretch=True, width=70)
                treeview.heading(column, text=column)
            heading_setted = True
            continue
        treeview.insert('', tk.END, iid=str(nrow), text=str(nrow),
            values=[cell.value for cell in row], tag=('odd' if nrow%2==0 else 'even',))

    scroll_vert.pack(side=tk.RIGHT, fill=tk.Y)
    scroll_hori.pack(side=tk.BOTTOM, fill=tk.X)
    treeview.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    treeview.tag_configure('odd', background='#FFF') #'#EFEFEF')
    treeview.tag_configure('even', background='#EAECEE') #'#D5D5D5')
    treeview.config(yscrollcommand=scroll_vert.set, xscrollcommand=scroll_hori.set)
    scroll_vert.config(command=treeview.yview)
    scroll_hori.config(command=treeview.xview)

    pop.transient(app)
    pop.grab_set()
    pop.focus()
    if nrow > 0:
        treeview.focus_set()
        treeview.selection_set(1)
        treeview.focus('1')

    pop.protocol("WM_DELETE_WINDOW", _pop_close)

def process_images(app):
    # data: east_upper_right,north_upper_right,east_lower_left,north_lower_left
    args = [app.east_upper_right.get(), app.north_upper_right.get(),
            app.east_lower_left.get(), app.north_lower_left.get()]

    if sum(map(lambda v: 1 if v==0 else 0, args)) > 0:
        showerror("Error", "Must set upper-right and lower-left\ncoordinates of images reference.")
        return
    last_dir = os.getcwd()
    os.chdir(app.prj_path.get())
    process = tk.Toplevel(app)
    scr_size = (app.winfo_screenwidth(), int(0.9*app.winfo_screenheight()))
    win_size = (width,height) = (250,70)
    win_pos = (x_pos,y_pos) = tuple(map(pos_center,scr_size,win_size,(2,2)))
    process.geometry(f'{width}x{height}+{x_pos}+{y_pos}')
    process.resizable(False, False)
    process.title("Processing images...")
    app.config(cursor='wait')
    process.config(cursor='wait')
    nfiles = len(app.files)
    step = tk.DoubleVar()
    data = tk.StringVar()
    foot = tk.StringVar()
    lbl1 = tk.Label(process, textvariable=data)
    lbl1.pack(side=tk.TOP, fill=tk.X)
    lbl2 = tk.Label(process, textvariable=foot)
    lbl2.pack(side=tk.BOTTOM, fill=tk.X)
    prog_bar = ttk.Progressbar(process, orient=tk.HORIZONTAL, length=nfiles,
        mode='determinate', variable=step, maximum=nfiles)
    prog_bar.pack(fill=tk.X, padx=10)
    process.transient(app)
    process.grab_set()
    process.protocol('WM_DELETE_WINDOW', None)
    process.focus()
    lista_dataBase = [] #list to save parameters
    (factor_area,factor_dist)=GI.georef(cv2.imread(app.files[0]), *args)
    GI.contourn = [GI.contourn1, GI.contourn2, None][app.method.get()]
    for i, file in enumerate(app.files):
        filename = os.path.basename(file)
        step.set(i)
        data.set("{:4.1f}% ({}/{})".format(100*i/nfiles, i+1, nfiles))
        foot.set("Processing Image {}".format(filename))
        app.status.set("Processing images...")
        app.update()
        #sleep(0.1)
        img = cv2.imread(file)
        (param,img_,cm) = GI.contourn(img, filename, factor_area, factor_dist,
            drawcontour=False, ellipse=False, hull_=False, shp=True, write=True) #island segmentation
        #clustering using k means
        (porce, center, label, res2) = GI.cluster(img_, filename,k=app.kval.get(), attemps=15,write=True)
        GI.legend(img_, filename, porce, center)
        #make a dataframe to storage set of parameters
        lista_dataBase.append(param)
    #DATABASE
    df_dataBase=pd.DataFrame(lista_dataBase)
    columns=['id','Major axis Length (km)','minor axis Length (km)','Area(ha)','Perimeter (km)','Centroid_X','Centroid_Y',
             'eX(km)', 'eY(km)','Circunference','Axe Orientationº','Wide(km)']
    df_dataBase.to_excel ('dataBase.xlsx',  index=False,header=columns)
    process.config(cursor='')
    app.config(cursor='')
    app.status.set("Ready: {} images processed correctly.".format(i+1))
    os.chdir(last_dir)
    app.processed.set(True)
    app.cont.config(state=tk.NORMAL)
    app.clus.config(state=tk.NORMAL)
    app.shap.config(state=tk.NORMAL)
    app.datab.config(state=tk.NORMAL)
    process.destroy()

def new_dataset(app):
    app.prj_path.set('')
    app.east_upper_right.set(0.0)
    app.north_upper_right.set(0.0)
    app.east_lower_left.set(0.0)
    app.north_lower_left.set(0.0)
    app.kval.set(6)
    app.method.set(1)
    app.processed.set(False)
    app.msg.set('')
    app.files = []
    app.children['!canvas'].delete(tk.ALL)
    app.children['!frame'].children['!listbox'].delete(0, tk.END)
    app.children['!frame'].children['!listbox'].last_select = None
    app.children['!button2'].config(state=tk.DISABLED)
    app.urce.config(state=tk.DISABLED)
    app.urcn.config(state=tk.DISABLED)
    app.llce.config(state=tk.DISABLED)
    app.llcn.config(state=tk.DISABLED)
    app.spin.config(state=tk.DISABLED)
    app.met1.config(state=tk.DISABLED)
    app.met2.config(state=tk.DISABLED)
    #app.met3.config(state=tk.DISABLED)
    app.img.config(state=tk.DISABLED)
    app.cont.config(state=tk.DISABLED)
    app.clus.config(state=tk.DISABLED)
    app.shap.config(state=tk.DISABLED)
    app.datab.config(state=tk.DISABLED)

def open_dataset(app):
    dir_path =  os.path.join(os.environ["USERPROFILE"], "Documents")
    #dir_path = os.path.dirname(os.path.abspath(__name__))
    app.status.set("Select a folder...")
    prj_path = askdirectory(initialdir=dir_path)
    if app is not None and len(prj_path) > 0:
        config_path = os.path.abspath(os.path.join(prj_path, 'config.json'))
        if not os.path.exists(config_path):
            showerror("Error", "Dataset configuration file does not exists.\nComplete new dataset and save it.")
            return
        with open(config_path, 'r') as fp:
            config = json.load(fp)
        app.prj_path.set(os.path.abspath(prj_path))
        #app.prj_path.set(os.path.abspath(os.path.join('..',config['prj_path'])))
        app.east_upper_right.set(float(config['east_upper_right']))
        app.north_upper_right.set(float(config['north_upper_right']))
        app.east_lower_left.set(float(config['east_lower_left']))
        app.north_lower_left.set(float(config['north_lower_left']))
        app.kval.set(int(config['kval']))
        app.method.set(int(config['method']))
        app.processed.set(config['processed'])
        app.files = []
        load_files(app)

def save_dataset(app):
    args = [app.east_upper_right.get(), app.north_upper_right.get(),
            app.east_lower_left.get(), app.north_lower_left.get()]
    if sum(map(lambda v: 1 if v==0 else 0, args)) > 0:
        showerror("Error", "Must set upper-right and lower-left\ncoordinates of images reference.")
        return

    if not len(app.files) > 0:
        showerror("Error", "Must load Dataset files to process.")
        return

    with open(os.path.join(app.prj_path.get(), 'config.json'), 'w') as fp:
        fp.write(json.dumps({
            'prj_path': os.path.basename(app.prj_path.get()),
            'east_upper_right': app.east_upper_right.get(),
            'north_upper_right': app.north_upper_right.get(),
            'east_lower_left': app.east_lower_left.get(),
            'north_lower_left': app.north_lower_left.get(),
            'kval': app.kval.get(),
            'method': app.method.get(),
            'processed': app.processed.get(),
            'files': list(map(os.path.basename, app.files))
        }))

def guide():
    guide = tk.Toplevel()
    guide.geometry("400x300")
    guide.title("Guide")
    def close(): guide.destroy()
    ttk.Button(guide, text="Close", command=close).pack(side=tk.BOTTOM, pady=5)
    tk.Text(guide).pack(padx=10, pady=5, fill=tk.BOTH, expand=True)

def about(app):
    app.option_add('Dialog.msg.font', 'Helvetica 12')
    showinfo(title="About", message='''
    pyGeoIsland v.2.3
    Copyright (c) 2020

    Conceptual Design and Theories:
    * Grisel Jiménez. [Universiti Teknologi PETRONAS]

    Technical Support:
    * Alejandro Sanchez. [ANMA Tecnología]:
    * Adolfo J. Cardozo S.. [ACAI Engineering ia]:
    ''')
    app.option_clear()
