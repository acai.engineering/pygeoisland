
#module to segment the entire island and apply the cluster
#Author: Grisel and Luis

#dependencies
import cv2
import numpy as np
import math
import os
import shapefile
import pandas as pd

param=[]

def georef (img,east_upper_right,north_upper_right,east_lower_left,north_lower_left):
    (h, w)=img.shape[:2]
    #factor areas
    area_pixeles = h * w #Area imagen
    w_real=abs(float(east_upper_right)-float(east_lower_left))
    h_real=abs(float(north_upper_right)-float(north_lower_left))
    area_real=w_real*h_real #Area Mapa
    factor_area=area_real/area_pixeles #factor de area
    #factor dist
    diagonal_pixeles=(w**2+h**2)**(0.5)
    diagonal_real=(w_real**2+h_real**2)**(0.5)
    factor_dist=diagonal_real/diagonal_pixeles
    return factor_area,factor_dist

#DRAW CONTOURNS to entire ISLAND Datasets1
def contourn1(img,filename,factor_area,factor_dist,drawcontour,ellipse,hull_,shp,write):
    (h_orig, w_orig)=img.shape[:2] # dimensiones de las imagenes originales
    img=cv2.resize(img,(600,600))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gauss = cv2.GaussianBlur(gray, (7,7), 0)
    rgb=cv2.resize(gauss,(680,420));img=cv2.resize(img,(680,420))
    ht, wt = rgb.shape[:2]
    ret,rgb = cv2.threshold(rgb,105,255,cv2.THRESH_BINARY)
    #Agrupar areas usando dilate
    kernel = np.ones((12,12),np.uint8)
    rgb = cv2.dilate(rgb,kernel,iterations = 1)
    #canny = cv2.Canny(rgb, 100, 140)
    contours, _ = cv2.findContours(rgb, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #para dibujar solo contornos que esten limitados por tamaño
    area_max=0
    for c in contours:
        area = cv2.contourArea(c)
        if area>area_max:
            area_max=area
            cm=c
    #dibujar contorno
    if drawcontour is True:
        cv2.drawContours(img, [cm], 0, (0, 0, 0), 1, cv2.LINE_AA)
    #mask = np.zeros_like(img) # Create mask where white is what we want, black otherwise
    mask = np.zeros(rgb.shape,np.uint8)
    cv2.drawContours(mask,[cm],0,255,-1)
    mask=255-mask
    #cv2.drawContours(mask,[cm],-1,(0, 0, 255),4)
    #cv2.imshow('cont', mask)
    mask = cv2.GaussianBlur(mask, (7,7), 0)
    (h,w)=img.shape[:2] # dimensiones de las imagenes
    #definicion de limites para recorrer la imagen
    [xmin,xmax]=[0,w]
    [ymin,ymax]=[0,h]
    img_=img
    #crear macara blanca
    for i in range(ymin,ymax):
        for j in range(xmin,xmax):
            if mask[i,j]==255:
                img_[i,j]=255
    #ejes y areas  y angulo
    (x, y), (MA, ma), angle=cv2.fitEllipse(cm)
    axe_orientation= 90 - angle if angle<90 else 270-angle
    e=cv2.fitEllipse(cm)
    if ellipse is True:
        cv2.ellipse(img_,e,(250,0,0),2)
    #convex
    lista_far=[]#lista para guardar las coordenadas de los puntos del poligono
    hull=cv2.convexHull(cm,returnPoints = False)
    defects = cv2.convexityDefects(cm,hull)
    for i in range(defects.shape[0]):
        s,e,f,d = defects[i,0]
        start = tuple(cm[s][0])
        end = tuple(cm[e][0])
        far = tuple(cm[f][0])
        lista_far.append(far)
        if hull_ is True:
            cv2.line(img_,start,end,[0,255,0],2)
            cv2.circle(img_,far,5,[0,0,255],-1)
    #determinar la x maxima
    max=0
    for punto in lista_far:
        if max <int(punto[0]):
             max=int(punto[0])
        else:
            max=max
    xmax=max
    #determinar la x minima
    xmin=min(lista_far)[0]
    wide=abs(xmax-xmin)
    #perimeter
    perimeter=cv2.arcLength(cm, closed=True)
    #centroide
    M = cv2.moments(cm)
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    # draw the contour and center of the shape on the image
    #cv2.circle(img_, (cX, cY), 7, (255, 255, 255), -1)
    #excentricity
    eX=abs(x-cX);eY=abs(y-cY)
    #circunferencia
    c=2*math.pi*math.sqrt ( (ma**2+MA**2)/2)
    #lista parametros
    param=[filename,ma*factor_dist,MA*factor_dist,area_max*factor_area,perimeter*factor_dist,cX*factor_dist,cY*factor_dist,eX*factor_dist,eY*factor_dist,c*factor_dist, axe_orientation,factor_dist*wide]
    #para crear imagen
    dir_path = os.getcwd()
    #escribir la imagen en la carpeta countourn
    if write is True:
        if not os.path.exists(os.path.join(dir_path, 'contourn')):
            os.mkdir('contourn')
        img_=cv2.resize(img,(w_orig,h_orig))
        cv2.imwrite(dir_path+'\\contourn\\poly_'+filename , img_)

    #print(cm,type(cm))
    if shp is True:
        w = shapefile.Writer('shapefiles/polygon_'+filename)
        w.field('id','C','40');w.field('Major_axis_Length_(km)','N',decimal=4);w.field('minor_axis_Length_(km)','N',decimal=4);w.field('Area(ha)','N',decimal=4)
        w.field('Perimeter_(km)','N',decimal=4);w.field('Centroid_X','N',decimal=4);w.field('Centroid_Y','N',decimal=4);w.field('eX(km)','N',decimal=4);w.field('eY(km)','N',decimal=4)
        w.field('Circunference','N',decimal=4);w.field('Axe_Orientationº','N',decimal=4);w.field('Wide(km)','N',decimal=4)
        n = cm.ravel()
        i=0;C=[]
        for j in n :
            if(i % 2 == 0):
                x = n[i]
                y = -1 * n[i + 1]
                coord=[x,y]
                C.append(coord)
            i=i+1
        C = [[p[0],p[1]+ht] for p in C.copy()]
        w.poly([C])
        w.record(filename,ma*factor_dist,MA*factor_dist,area_max*factor_area,perimeter*factor_dist,cX*factor_dist,cY*factor_dist,
                 eX*factor_dist,eY*factor_dist,c*factor_dist,axe_orientation,wide*factor_dist)
        #w.save('shapefiles/polygon_'+filename+'.shp')
        w.close()
    return param,img_,cm

#DRAW CONTOURNS to entire ISLAND dataset2
def contourn2(img,filename,factor_area,factor_dist,drawcontour,ellipse,hull_,shp,write):
    (h,w,b)=img.shape
    factor_amplia=1.02
    (h_big,w_big)=( int(h*factor_amplia),int(w*factor_amplia) )
    img_big = np.ones((h_big,w_big,3),np.uint8)*255 #crear la mascara blanca
    img_white=img_big

    #Quitar el oceano
    for i in range(h):
        for j in range(w):
            if img[i,j][0]==0 and img[i,j][1]==0 and img[i,j][2]==0:
                img[i,j]=255

    x_offset=int( (factor_amplia-1)*w/2)
    y_offset=int( (factor_amplia-1)*h/2)
    img_big[ y_offset:y_offset+img.shape[0], x_offset:x_offset+img.shape[1]] = img
    img=img_big

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gauss = cv2.GaussianBlur(gray, (7,7), 0)
    ret,bin = cv2.threshold(gauss,175,255,cv2.THRESH_BINARY)

    # obtener los contornos
    contours, _ = cv2.findContours(bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #para eliminar el rectangulo de maxima area y generar poligono isla
    AREAS=[]
    for c in contours:
        area = cv2.contourArea(c)
        AREAS.append(area)
    #eliminar el maximo contorno que es el rectangulo de la imagen
    area_max=0
    for n,a in enumerate(AREAS):
        if a>area_max:
            area_max=a; indice=n
    contours.pop(indice)
    area_max=0;
    for c in contours:
        area = cv2.contourArea(c)
        if area>area_max:
            area_max=area
            cm=c

    # dibujar los contornos
    if drawcontour is True:
        cv2.drawContours(img_big, [cm], -1, (0, 0, 255), 2, cv2.LINE_AA)
    #poner el oceano
    for i in range(h_big):
        for j in range(w_big):
            if img_big[i,j][0]==255 and img_big[i,j][1]==255 and img_big[i,j][2]==255:
                img_big[i,j]=[255,0,0]

    img_=img_big
    #ejes y areas  y angulo
    (x, y), (MA, ma), angle=cv2.fitEllipse(cm)
    axe_orientation= 90 - angle if angle<90 else 270-angle
    e=cv2.fitEllipse(cm)
    if ellipse is True:
        cv2.ellipse(img_,e,(250,0,0),2)
    #convex
    lista_far=[]#lista para guardar las coordenadas de los puntos del poligono
    hull=cv2.convexHull(cm,returnPoints = False)
    defects = cv2.convexityDefects(cm,hull)
    for i in range(defects.shape[0]):
        s,e,f,d = defects[i,0]
        start = tuple(cm[s][0])
        end = tuple(cm[e][0])
        far = tuple(cm[f][0])
        lista_far.append(far)
        if hull_ is True:
            cv2.line(img_,start,end,[0,255,0],2)
            cv2.circle(img_,far,5,[0,0,255],-1)
    #determinar la x maxima
    max=0
    for punto in lista_far:
        if max <int(punto[0]):
             max=int(punto[0])
        else:
            max=max
    xmax=max
    #determinar la x minima
    xmin=min(lista_far)[0]
    wide=abs(xmax-xmin)
    #perimeter
    perimeter=cv2.arcLength(cm, closed=True)
    #centroide
    M = cv2.moments(cm)
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    # draw the contour and center of the shape on the image
    #cv2.circle(img_, (cX, cY), 7, (255, 255, 255), -1)
    #excentricity
    eX=abs(x-cX);eY=abs(y-cY)
    #circunferencia
    c=2*math.pi*math.sqrt ( (ma**2+MA**2)/2)
    #lista parametros
    param=[filename,ma*factor_dist,MA*factor_dist,area_max*factor_area,perimeter*factor_dist,cX*factor_dist,cY*factor_dist,eX*factor_dist,eY*factor_dist,c*factor_dist, axe_orientation,factor_dist*wide]
    #para crear imagen
    dir_path = os.getcwd()
    #escribir la imagen en la carpeta countourn
    if write is True:
        if not os.path.exists(os.path.join(dir_path, 'contourn')):
            os.mkdir('contourn')
        #img_=cv2.resize(img,(w_orig,h_orig))
        cv2.imwrite(dir_path+'\\contourn\\poly_'+filename , img_)

    #print(cm,type(cm))
    if shp is True:
        w = shapefile.Writer('shapefiles/polygon_'+filename)
        w.field('id','C','40');w.field('Major_axis_Length_(km)','N',decimal=4);w.field('minor_axis_Length_(km)','N',decimal=4);w.field('Area(ha)','N',decimal=4)
        w.field('Perimeter_(km)','N',decimal=4);w.field('Centroid_X','N',decimal=4);w.field('Centroid_Y','N',decimal=4);w.field('eX(km)','N',decimal=4);w.field('eY(km)','N',decimal=4)
        w.field('Circunference','N',decimal=4);w.field('Axe_Orientationº','N',decimal=4);w.field('Wide(km)','N',decimal=4)
        n = cm.ravel()
        i=0;C=[]
        for j in n :
            if(i % 2 == 0):
                x = n[i]
                y = -1 * n[i + 1]
                coord=[x,y]
                C.append(coord)
            i=i+1
        C = [[p[0],p[1]+h_big] for p in C.copy()]
        w.poly([C])
        w.record(filename,ma*factor_dist,MA*factor_dist,area_max*factor_area,perimeter*factor_dist,cX*factor_dist,cY*factor_dist,
                 eX*factor_dist,eY*factor_dist,c*factor_dist,axe_orientation,wide*factor_dist)
        #w.save('shapefiles/polygon_'+filename+'.shp')
        w.close()
    return param,img_,cm

#CLUSTER USING K MEANS
def cluster(img_contour,filename, k, attemps,write):
    img2=img_contour.reshape( (-1,3) )
    img2=np.float32(img2)
    # Define criteria = ( type, max_iter = 10 , epsilon = 1.0 )
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    ret,label,center=cv2.kmeans(img2,k,None,criteria,attemps,cv2.KMEANS_RANDOM_CENTERS)
    center=np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img_contour.shape))
##    #colorear pixeles blancos a azul
##    for i in range(len(center)):
##        if int(sum(center[i]))==765:
##            center[i]=[255,0,0]
    #contar los pixeles
    PORCE=[]
    for pixel in range (k):
        porcentaje=round((list(label).count(pixel))/len(label)*100)
        PORCE.append(porcentaje)

    #print(PORCE)
    #para grabar la imagen
    if write is True:
        dir_path = os.getcwd()
        if not os.path.exists(os.path.join(dir_path, 'cluster')):
            os.mkdir('cluster')
        cv2.imwrite(dir_path+'\\cluster\\cluster_'+filename , res2)
    return PORCE,center, label, res2

def legend(img_contour,filename,PORCE,center):
    (h,w)=img_contour.shape[:2] # dimensiones de las imagenes
    cv2.rectangle(img_contour,(int(w*.77)-5,int(h*.83)),(int(w*.87)+3,int(h*.92)+15),(255,255,255),-1)
    posy=0
    lista_color=[ list(color_) for color_ in center]
    for clas,color in zip(PORCE,lista_color):
        posy+=11
        cv2.putText(img_contour,str(clas)+'%',(int(w*.79),int(h*.84+posy)),
                    cv2.FONT_HERSHEY_COMPLEX, 0.4, (5,0,0), 1, lineType=cv2.LINE_AA)
        cv2.rectangle(img_contour,(int(w*.77),int(h*.84+posy)-9),(int(w*.77)+8,int(h*.84+posy)+2),[int(c) for c in color],-1)

    dir_path = os.getcwd()
    if not os.path.exists(os.path.join(dir_path, 'cluster')):
        os.mkdir('cluster')
    cv2.imwrite(dir_path+'\\cluster\\cluster_'+filename , img_contour)


